"""
GeneticDE

This is a simple module to test the result of [TL] TSOULOS, Ioannis G. et
LAGARIS, Isaac E. "Solving differential equations with genetic programming".
Genetic Programming and Evolvable Machines, 2006, vol. 7, no 1, p. 33-54.
"""

#TODO:
# * If string expression contains 'inf', give it a constant fitness
# * change typr(...) != ... to isinstance(..., ...)

import re
import random
import sympy
import numpy
import copy
import matplotlib.pyplot as plt
import warnings
# for time-limited execution:
from contextlib import contextmanager
import threading
import _thread

# To avoid a gmpy2 bug: https://github.com/aleaxit/gmpy/issues/280
import os
os.environ['MPMATH_NOGMPY'] = '1'

# we only care if an actual error occurs
numpy.seterr(all='ignore')

class Grammar:
    """
    Contains BNF-grammar used to parse expressions.

    The default grammar is inspired by the one present in [TL], but an input
    grammar can be given as a BNF-grammar in the style of a dictionary. See
    the default: Grammar().grammar for details.
    """

    def __init__(self, grammar=None):
        if grammar == None:
            grammar = {
                '{expr}' : [
                    '{expr} {op} {expr}',
                    '({expr})',
                    '{func}({expr})',
                    '{digit}',
                    'x'
                ],
                '{op}'   : [
                    '+',
                    '-',
                    '*',
                    '/'
                ],
                '{func}' : [
                    'sin',
                    'cos',
                    'exp',
                    'log'
                ],
                '{digit}': [ f'{i}.0' for i in range(10)]
            }
        self.grammar = grammar

        # Verify that the grammar is complete (all '{}' expressions present in
        # values are present as keys)
        es = set([ m for e in [ self.grammar[i] for i in self.grammar] for s in e for m in re.findall(r'\{[a-z]*\}', s)])
        if es.difference([ i for i in self.grammar]) != set():
            raise SyntaxError(f'Grammar is incomplete and is missing the following element(s): {es.difference([ i for i in self.grammar])}')

    def __str__(self):
        st = 'S :== <expr> (0)\n'
        for g in self.grammar:
            st += f'\n{g} ::= '
            count = 0
            ln = max([len(v) for v in self.grammar[g]])
            for v in self.grammar[g]:
                if count == 0:
                    st += f'  {v}' + (ln-len(v)+2)*' ' + f'({count})\n'
                else:
                    st += len(f'{g} ::= ')*' ' + f'| {v}' + (ln-len(v)+2)*' ' + f'({count})\n'
                count += 1
        return st

class DifferentialEquation:
    """
    Given an input function (see below), this carries a differential equation.

    The 'input function' is designed for example as:

    >>> import sympy
    >>> x = sympy.Symbol('x')
    >>> input_function = lambda expr: sympy.diff(expr, x) - (2*x - expr)/x
    >>> de = DifferentialEquation(input_function)

    which here would represent the DE "y'(x) - (2x - y(x))/x = 0".

    Optionally you can also provide:

    domain: Domain over where to attempt to solve the DE (solving function
    should be regular in this domain), for example a NumPy linspace.
    expression: SymPy expression on which the unknown function in the DE will be
    evaluated. See also the method `make`.

    Calling the DE given that it has been provided an expression, will generate
    the deviation from zero at that point. That is continuing the example above:

    >>> de.make(x**2)
    >>> de(0.25)
    -1.25
    """

    def __init__(self, input_function, domain=None, expression=None):

        self.input_function = input_function
        if type(domain) == type(None):
            domain = numpy.linspace(0.1,numpy.pi,num=10)
        self.domain = domain

        self.x = sympy.Symbol('x')
        self.de_expression = self.input_function(sympy.Function('y')(self.x))

        if expression != None:
            self.make(expression)

    def __str__(self):
        return f'Differential Equation: {self.de_expression}'

    def __call__(self, x):
        if not hasattr(self, 'evaluated_expression') or not hasattr(self, 'call_function'):
            raise AttributeError('To call a DE you need to run the `make` method first, or provide an `expression` when initiating the object.')

        # Try and always provide a number returned
        try:
            return self.call_function(x)
        except ZeroDivisionError:
            warnings.warn(f'DE ({self.de_expression}) evaluated on {self.expression} hit a singularity at x={x}.')
            return 10.0**8

    def make(self, expression):
        """Given an input (sympy) expression, the DE is evaluated on that expression."""

        self.expression = expression

        # The sympy expression may be evaluated to a singular expression in
        # different ways.

        # Evaluate the DE on the expression
        try:
            with time_limit(0.1, 'sleep'):
                self.evaluated_expression = self.input_function(expression)
        except (OverflowError, TimeoutException):
            self.evaluated_expression = sympy.sympify('10.0**8')
        except:
            raise ValueError(f'Expression failed to evaluate on DE: {expression}')

        # Create a function that we can use for calls, and calculate the fitness
        # value while we are at it
        try:
            with time_limit(0.1, 'sleep'):
                self.call_function = sympy.lambdify('x', self.evaluated_expression)
                self.value = sum([ abs(self(x)) for x in self.domain ])**2
        except (KeyError, OverflowError, ZeroDivisionError, TimeoutException):
            self.call_function = lambda x: 10.0**4
            self.value = sum([ abs(self(x)) for x in self.domain ])**2
        except TypeError:
            print(f'caught type error for: {self.expression}')
            print(f'{[(a, type(a)) for a in self.expression.atoms()]}')
            # # log(exp(inf*log(x)/log(-x + 20.7425235219065))) + 6.0
            raise TypeError()


class BoundaryCondition:
    """
    Given list of input functions and points, this carries boundary conditions.

    The input functions and points are paired expressions and points at which
    the expressions should be enforced, for example:

    >>> bc = BoundaryCondition([lambda expr: expr - 20.1], [0.1])

    represents the BC "y(0.1) - 20.1 == 0".

    There is no limit to the number of boundary conditions you can give, meaning
    you are free to try and solve a potentially overdetermined system.

    Optionally you can also provide:

    expression: SymPy expression on which the unknown function in the BC will be
    evaluated. See also the method `make`.

    Calling the BC given that it has been provided an expression, will generate
    the deviation from zero at that point, or at each point for each BC. That is
    continuing the example above:

    >>> from sympy import *
    >>> x = symbols("x")
    >>> bc.make(x**2)
    >>> bc(1.0)
    [-19.1]
    >>> bc([1.0])
    [-19.1]
    """

    def __init__(self, input_functions, points, expression=None):

        if type(input_functions) != list or type(points) != list:
            raise TypeError('The boundary condition has to be set up with a list of input functions and a list of points where is is to be evaluated.')

        if len(input_functions) != len(points):
            raise ValueError('The number of functions do not match the number of points.')

        self.input_functions = input_functions
        self.points = points

        self.x = sympy.Symbol('x')
        self.bc_expression = [ f(sympy.Function('y')(self.x)) for f in self.input_functions ]

        if expression != None:
            self.make(expression)

    def __str__(self):
        st = 'Boundary Conditions:'
        for i in range(len(self.points)):
            st += f'\n {self.bc_expression[i].subs(sympy.Symbol("x"),self.points[i])} = 0'
        return st

    def __call__(self, x):
        if not hasattr(self, 'evaluated_expressions') or not hasattr(self, 'call_functions'):
            raise AttributeError('To call a BC you need to run the `make` method first.')

        # If the input is a list, evaluate point by point.
        if type(x) == list:
            if len(x) != len(self.call_functions):
                raise ValueError('The number of functions does not match the number of points')

            try:
                return [ self.call_functions[i](x[i])**2 for i in range(len(x)) ]
            except (OverflowError, ZeroDivisionError):
                return [ 10.0**8 for i in range(len(x)) ]

        # If it is not a list, evaluate at the given point
        return [ f(x) for f in self.call_functions ]

    def make(self, expression):
        """Given an input (sympy) expression, the BC is evaluated on that expression."""
        try:
            with time_limit(0.1, 'sleep'):
                self.evaluated_expressions = [ f(expression) for f in self.input_functions ]
        except TimeoutException:
            self.evaluated_expressions = [ sympy.sympify('10**8') for f in self.input_functions ]

        try:
            with time_limit(0.1, 'sleep'):
                self.call_functions = [ sympy.lambdify('x', ex) for ex in self.evaluated_expressions ]
        except (KeyError, OverflowError, ZeroDivisionError, TimeoutException):
            self.call_functions = [ lambda x: 10.0**8 for _ in self.evaluated_expressions ]
        finally:
            self.values = self(self.points)

class Individual:
    """
    Represents an individual that will part take in the genetic evolution.

    Individuals needs to be initiated with a differential equation and boundary
    condition, which it is a candidate to solve.

    You can tune the default settings for the individual:

    chromosome: A list of integers that represent the mathematical expression
    which is generated by the BNF-grammar provided. (overrides `size` and
    `width`) (default: random)
    grammar: A BNF-grammar, see "Grammar" (default: Grammar())
    size: Length of the chromosome (if not provided manually) (default: 50)
    width: The maximum number a chromosome entry can take (if not provided
    manually) (default: 256)
    weight: A weight factor that is assigned to the boundary condition, when
    calculating the fitness value. (default: 100)

    Two individuals are considered equal if the attributes fitness, active
    (number of entries on the chromosome that are used to generate the
    mathematical expression), expression_string, and expression are the same.

    # Fitness

    The fitness is calculated by the weighted sum of an error and a penalty. The
    error is the failure to solve the DE (DifferentialEquation().value) and
    penalty is the failure to solve the BC (BoundaryCondition().value):

    fitness = error + weight*penalty
    """

    def __init__(self, de, bc, chromosome=None, grammar=None, size=50, width=256, weight=100, crossover='one_point'):
        # Differential (or algebraic) equation to be solved
        self.de = copy.copy(de)
        # Boundary condition to be sovled
        self.bc = copy.copy(bc)

        if grammar == None:
            grammar = Grammar()
        if type(grammar) != Grammar:
            raise TypeError(f'Provided grammar was of wrong type {type(grammar)} when Grammar was expected.')
        self.grammar = grammar
        self.size = size
        self.width = width
        self.penalty_weight = weight

        # Generate a random chromosome string
        if chromosome == None:
            self.chromosome = [ random.randint(0,width) for _ in range(self.size) ]
        else:
            self.chromosome = chromosome
            self.size = len(self.chromosome)

        self.crossover_name = crossover
        self.crossover_operators = ['one_point', 'two_point']
        self.crossover_select()

        # Generate sympy expression and fitness
        self._update_everything()

    def __str__(self):
        return f'Individual: {self.expression_string} (fitness: {self.fitness})'

    def __iter__(self):
        self.idx = 0
        return self

    def __next__(self):
        try:
            ret = self.chromosome[self.idx]
        except IndexError:
            raise StopIteration()
        self.idx += 1
        return ret

    def __getitem__(self, idx):
        return self.chromosome[idx]

    def __setitem__(self, i, elem):
        self.chromosome[i] = elem
        # update expression
        self._update_everything()

    def __add__(self, other):
        """Shorthand operator for crossover. Returns two crossed-over individuals."""
        if type(other) != Individual:
            raise NotImplementedError(f'Tried to compare "{type(self)}" to "{type(other)}". Addition only between two individuals.')
        return self.crossover(other)

    def crossover_select(self):
        if not self.crossover_name in self.crossover_operators:
            raise NotImplementedError(f'Tried to select crossover "{self.crossover_name}". Only the following are available: {self.crossover_operators}')

        self.crossover = eval('self.' + self.crossover_name + '_crossover')

    def one_point_crossover(self, other):
        # choose a cut among actives if possible, even if just one active
        cut = min([self.active, other.active])
        if cut < 2:
            cut = max([self.active, other.active])
            if cut < 2:
                # one active
                cut = random.randint(1,self.size-1)
            else:
                # no actives
                cut = random.randint(1,cut-1)
        else:
            # both actives
            cut = random.randint(1,cut-1)
        # perform crossover
        ni1, ni2 = self.chromosome[0:cut] + other.chromosome[cut:], other.chromosome[0:cut] + self.chromosome[cut:]
        ni1, ni2 = Individual(self.de, self.bc, chromosome=ni1, size=self.size, width=self.width), Individual(self.de, self.bc, chromosome=ni2, size=self.size, width=self.width)

        return ni1, ni2

    def two_point_crossover(self, other):

        # XXX complete this

        ni1, ni2 = Individual(self.de, self.bc, chromosome=ni1, size=self.size, width=self.width), Individual(self.de, self.bc, chromosome=ni2, size=self.size, width=self.width)

        return ni1, ni2

    def __eq__(self, other):
        if type(other) != Individual:
            raise NotImplementedError(f'Tried to compare "{type(self)}" to "{type(other)}". Equalities only between two individuals.')
        return self.expression_string == other.expression_string
        if self.fitness == other.fitness or self.expression_string == other.expression_string or self.expression == other.expression:
            return True
        if self.fitness != other.fitness:
            return False
        if self.active != other.active:
            return False
        if self.expression_string != other.expression_string:
            return False
        if self.expression != other.expression:
            return False
        return True
        #return self.expression_string == other.expression_string

    def __call__(self, x):
        if not hasattr(self, 'function'):
            self._generate_function()

        return self.function(x)

    def _generate_expression_string(self, chromosome=None, working_expr=None, depth=0):
        """Returns a string of a mathematical expression generated from the individuals chromosome."""

        if chromosome == None:
            first_run = True
            chromosome = self.chromosome
        else:
            first_run = False

        if working_expr == None:
            working_expr='{expr}'

        counter = 0

        while re.search(r'\{[a-z]*\}', working_expr) != None and counter < len(chromosome):

            # get the first matching '{}'
            first = re.search(r'\{[a-z]*\}', working_expr)

            # look up replacement string
            rep = self.grammar.grammar[first.group()]
            rep = rep[chromosome[counter] % len(rep)]

            # replace first
            new_string = re.sub(first.group(), rep, first.string, count=1)

            # prepare for next step
            working_expr = new_string
            counter += 1

        if first_run:
            self.active = counter

        # If the loop terminates early, this fixes that.
        # TODO: add a recursion depth detection...?
        if re.search(r'\{[a-z]*\}', working_expr) != None:
            working_expr = self._generate_expression_string(list(map(lambda x: x + 1, chromosome)), working_expr, depth+1)

        return working_expr

    def _generate_expression(self):
        """Generates a sympy expression built from the chromosome."""
        if not hasattr(self, 'expression_string'):
            self.expression_string = self._generate_expression_string()

        try:
            with time_limit(0.1, 'sleep'):
                try:
                    self.expression = sympy.sympify(self.expression_string)
                except MemoryError:
                    self.expression = sympy.sympify('10.0**8')
                if (sympy.numbers.Float('inf') or -sympy.numbers.Float('inf') or sympy.numbers.NaN) in self.expression.atoms():
                    # 'inf' and 'nan' causes TypeErrors in the DE, so remove if present
                    self.expression = sympy.sympify('10.0**8')
        except (OverflowError, ValueError):
            #warnings.warn(f'Sympyification failed: {self.expression_string}. Function values not trustable.')
            self.expression = sympy.sympify('10.0**8')
        except TimeoutException:
            #warnings.warn(f'Sympyification failed due to timeout: {self.expression_string}. Function values not trustable.')
            self.expression = sympy.sympify('10.0**8')

    def _generate_function(self):
        """Generates a function for the expression."""
        if not hasattr(self, 'expression'):
            self._generate_expression()
        try:
            with time_limit(0.1, 'sleep'):
                self.function = sympy.lambdify('x', self.expression)
        except (KeyError, OverflowError, ZeroDivisionError, RecursionError):
            #warnings.warn(f'Lambdification failed: {self.expression}. Function values not trustable.')
            self.function = lambda x: 10.0**8
        except TimeoutException:
            #warnings.warn(f'Lambdification failed due to timeout: {self.expression}. Function values not trustable.')
            self.function = lambda x: 10.0**8


    def _update_everything(self):
        """Call if the chromosome has changed to update everything from expressions to fitness."""

        self.expression_string = self._generate_expression_string()
        self._generate_expression()
        self._generate_function()

        self.de.make(self.expression)
        self.bc.make(self.expression)

        self.error = self.de.value
        self.penalty = sum([ abs(v) for v in self.bc.values ])
        self.fitness = self.error + self.penalty_weight * self.penalty

        if f'{self.fitness}' == 'nan' or f'{self.fitness}' == 'inf':
            self.fitness = 10.0**8

class Population:
    """
    The population to be used for solving a DE.

    The population needs to be initiated with a differential equation and a set
    of boundary conditions. The others have defaults.

    The optional settings for the population is:

    individuals: A list of individuals (default: random)
    popsize: The number of individuals that should be generated (default: 100)
    size: The length of the chromosomes the individuals should have (default: 50)
    width: The maximum value of the entries of the chromosomes (default: 256)
    s: replication rate, meaning popsize*(1-s) (rounded to even number)
    offspring will be produced at crossover (default: .2).
    mr: mutation rate, meaning for each entry if a uniformly random number
    between (0,1) is lower than the mutation rate, that entry will change
    randomly (default: 0.05)
    """

    def __init__(self, de, bc, individuals = None, popsize=1000, size=50, width=256, s=.2, mr=.05):

        self.de = copy.copy(de)
        self.bc = copy.copy(bc)

        self.popsize = popsize
        self.size = size
        self.width = width
        self.replication_rate = s
        self.mutation_rate = mr

        if individuals == None:
            self.individuals = []
            while len(self.individuals) < popsize:
                indi = Individual(self.de,self.bc,size=self.size,width=self.width)
                # Make sure we only add "new" individuals
                if indi not in self.individuals:
                    self.individuals.append(indi)
            self.individuals = sorted(self.individuals, key=lambda x: x.fitness)
        else:
            self.individuals = sorted(individuals, key=lambda x: x.fitness)
            self.popsize = len(self.individuals)
            self.size = self.individuals[0].size
            self.width = self.individuals[0].width

        self.best = self.individuals[0]

    def __str__(self):
        return f'Population of {self.popsize} individuals. Best candidate: {self.best.expression_string} (fitness: {self.best.fitness})'

    def __iter__(self):
        self.idx = 0
        return self

    def __next__(self):
        try:
            ret = self.individuals[self.idx]
        except IndexError:
            raise StopIteration()
        self.idx += 1
        return ret

    def __getitem__(self, idx):
        return self.individuals[idx]

    def tournament(self, pop):
        """Using input population, select one individual using tournament."""
        tpop = random.sample(pop, random.randint(2,len(pop)))
        return sorted(tpop, key=lambda x: x.fitness)[0]

    def populate_tournament(self):
        """Generate a population for crossover using tournament."""
        pop_copy = copy.copy(self.individuals)
        pop_ret = []
        crosssize = (lambda x: x if x % 2 == 0 else x-1)(round(len(pop_copy)*(1-self.replication_rate)))
        while len(pop_ret) < crosssize:
            indi = self.tournament(pop_copy)
            pop_copy.remove(indi)
            pop_ret += [indi]
        return sorted(pop_ret, key=lambda x: x.fitness)

    def get_cross_and_mut_pop(self):
        """Returns two populations: one to use for corssover and one for mutation."""
        self.crosspop = self.populate_tournament()
        self.mutpop = sorted([ i for i in self.individuals if i not in self.crosspop ], key=lambda x: x.fitness)

    def mutate(self):
        """Returns a new mutated version of the input population."""
        if not hasattr(self, 'mutpop'):
            self.get_cross_and_mut_pop()

        retpop = [ Individual(self.de, self.bc, chromosome=[ random.randint(0,self.width) if random.random() < self.mutation_rate else c for c in i ], size=self.size, width=self.width) for i in self.mutpop]

        # If the elements have not been mutated in the active genes, it has not
        # changed, so we need to make sure it has changed
        for i in range(len(retpop)):
            if retpop[i][:retpop[i].active] == self.mutpop[i][:self.mutpop[i].active]:
                # We can still end up with the same expression even if the chromosomes
                # are different if they have the equivalent muduli the length of
                # that sub-grammar, so loop until fixed.
                rc = random.choice(range(retpop[i].active))
                while retpop[i].expression_string == self.mutpop[i].expression_string:
                    retpop[i][rc] = random.randint(0,self.width)

        return retpop

    def crossover(self):
        """Returns a new crossed-over (one-point) version of the input population."""
        if not hasattr(self, 'crosspop'):
            self.get_cross_and_mut_pop()
        pop_copy = copy.copy(self.crosspop)
        pop_crossed = []
        while len(pop_copy) > 1:
            # select two idividuals for crossover
            i1,i2 = random.sample(pop_copy, 2)
            while i1 == i2 and not len(collect(pop_copy)) == 1:
                i1,i2 = random.sample(pop_copy, 2)
            pop_copy.remove(i1), pop_copy.remove(i2)
            ni1, ni2 = i1 + i2
            pop_crossed.append(ni1), pop_crossed.append(ni2)

        pop_crossed = sorted(pop_crossed, key=lambda x: x.fitness)
        return pop_crossed

    def next(self):
        """Returns a new population that contains the next generation."""
        # [TL] says: 'The new individuals will replace the worst ones in the population at the end of the crossover', so independently of their fitness.
        # Also: 'The mutation operation is applied to every chromosome excluding those which have been selected for replication in the next generation.'
        # what that means is not clear, as we are working at the present generation.
        c = self.crossover()
        m = self.mutate()
        totnew = sorted(self.individuals[:(self.popsize - len(self.crosspop))] + c + m, key=lambda x: x.fitness)

        return Population(self.de, self.bc, individuals=totnew[:self.popsize])

# https://stackoverflow.com/questions/366682/how-to-limit-execution-time-of-a-function-call-in-python
class TimeoutException(Exception):
    def __init__(self, msg=''):
        self.msg = msg

@contextmanager
def time_limit(seconds, msg=''):
    timer = threading.Timer(seconds, lambda: _thread.interrupt_main())
    timer.start()
    try:
        yield
    except KeyboardInterrupt:
        raise TimeoutException("Timed out for operation {}".format(msg))
    finally:
        # if the action ends in specified time, timer is canceled
        timer.cancel()

def collect(ls):
    retlist = []
    for l in ls:
        if l not in retlist:
            retlist.append(l)
    return retlist
