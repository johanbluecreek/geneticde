#!/usr/bin/env julia

using UnicodePlots

sol = ARGS[1]
sol = replace(sol, "**" => "^")

expr = ARGS[2]
expr = replace(expr, "**" => "^")

domain = eval(Meta.parse(ARGS[3]))

fun_sol = eval(Meta.parse("x -> " * sol))
fun_expr = eval(Meta.parse("x -> " * expr))

fun = x -> fun_sol(x) - fun_expr(x)

# compare-plot

plt = lineplot(fun_sol, domain..., title="Compare-plot", name="Solution")
plt = lineplot!(plt, fun_expr, domain..., name="Candidate")

plt |> x -> println(IOContext(stdout, :color=>true), x)

"" |> println

# diff-plot

plt = lineplot(fun, domain..., title="Diff-plot", name="Difference")

plt |> x -> println(IOContext(stdout, :color=>true), x)

