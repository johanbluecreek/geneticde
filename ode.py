#!/usr/bin/env python3

from GeneticDE import *
import sys
import subprocess

import time

x = sympy.Symbol('x')

def loopit(de,bc,sol,domain):

    test_results = []

    for test in range(30):

        pop = Population(de,bc,s=.8)
        while pop.best.fitness < 10.0**-5:
            pop = Population(de,bc,s=.8)

        counter = 0
        best = pop.best
        diversity = len(collect(pop.individuals))
        meanf = sum([i.fitness for i in pop])/len(pop.individuals)
        print(f'Search #{test+1} starting.')
        print(f'Starting best ({counter}):\n{pop.best}')
        p = subprocess.Popen(
            ['julia', 'uniplot.jl', sol, pop.best.expression_string, f'{domain}']
        , shell=False)
        p.communicate()
        print(f'Starting diversity: {diversity}')
        print(f'Starting mean fitness: {meanf}')
        if counter % 10 == 0:
            print(f'-- Top 10 ({counter}): --')
            for i in pop[:10]:
                print(i)
            print('--')
        while counter < 2000 and best.fitness > 10.0**-10:
            with time_limit(60*60.0, 'end'):
                pop = pop.next()
            counter += 1
            if counter % 10 == 0:
                print(f'-- Top 10 ({counter}): --')
                for i in pop[:10]:
                    print(i)
                print('--')
            if pop.best != best:
                best = pop.best
                print(f'Current best ({counter}):\n{pop.best}')
                p = subprocess.Popen(
                    ['julia', 'uniplot.jl', sol, pop.best.expression_string, f'{domain}']
                , shell=False)
                p.communicate()
            if diversity != len(collect(pop.individuals)):
                diversity = len(collect(pop.individuals))
                print(f'Current diversity ({counter}): {diversity}')
            if meanf != sum([i.fitness for i in pop])/len(pop.individuals):
                meanf = sum([i.fitness for i in pop])/len(pop.individuals)
                print(f'Current mean fitness ({counter}): {meanf}')

        print(f'Search #{test+1} ended.\n')

        test_results.append((counter, copy.copy(pop.best)))

    return test_results

if __name__ == '__main__':

    ode1_sol = 'x + 2/x'
    ode1_domain = (0.1,1.0)

    ode1 = DifferentialEquation(
        lambda expr: sympy.diff(expr, x) - (2*x - expr)/x,
        domain=numpy.linspace(*ode1_domain,num=10)
    )
    ode1_bc = BoundaryCondition(
        [lambda expr: expr - 20.1],
        [0.1]
    )

    #test_results = loopit(ode1, ode1_bc, ode1_sol, ode1_domain)
    #for t in test_results:
    #    print(f'{t[0]}:')
    #    print(t[1])
    #print('')
    #print(f'min: {min([t[0] for t in test_results])} | max: {max([t[0] for t in test_results])} | avg: {sum([t[0] for t in test_results])/len(test_results)}')

    ode2_sol = '(x+2)/sin(x)'
    ode2_domain = (0.1,1.0)

    ode2 = DifferentialEquation(
        lambda expr: sympy.diff(expr, x) - (1 - expr * sympy.cos(x))/sympy.sin(x),
        domain=numpy.linspace(*ode2_domain,num=15)
    )
    ode2_bc = BoundaryCondition(
        [lambda expr: expr - 2.1/sympy.sin(0.1)],
        [0.1]
    )

    test_results = loopit(ode2, ode2_bc, ode2_sol, ode2_domain)
    for t in test_results:
        print(f'{t[0]}:')
        print(t[1])
        print('')
    print(f'min: {min([t[0] for t in test_results])} | max: {max([t[0] for t in test_results])} | avg: {sum([t[0] for t in test_results])/len(test_results)}')